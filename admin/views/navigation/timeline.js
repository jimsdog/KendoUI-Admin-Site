$(function () {
    // 垂直单侧时间线
    $('#verticalOneSideTimeline').kendoTimeline({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/timeline.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                data: 'data',
                model: {
                    fields: {
                        date: { type: 'date' }
                    }
                }
            }
        },
        dateFormat: 'yyyy-MM-dd',
        eventWidth: 'auto',
        alternatingMode: false,
        orientation: 'vertical'
    });
    // 垂直双侧时间线
    $('#verticalBothSidesTimeline').kendoTimeline({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/timeline.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                data: 'data',
                model: {
                    fields: {
                        date: { type: 'date' }
                    }
                }
            }
        },
        dateFormat: 'yyyy-MM-dd',
        eventWidth: 'auto',
        alternatingMode: true,
        orientation: 'vertical'
    });
    // 水平时间线
    $('#horizontalTimeline').kendoTimeline({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/timeline.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                data: 'data',
                model: {
                    fields: {
                        date: { type: 'date' }
                    }
                }
            }
        },
        dateFormat: 'yyyy-MM-dd',
        eventHeight: 360,
        orientation: 'horizontal'
    });
    // 折叠事件时间线
    $('#collapsibleTimeline').kendoTimeline({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/timeline.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                data: 'data',
                model: {
                    fields: {
                        date: { type: 'date' }
                    }
                }
            }
        },
        dateFormat: 'yyyy-MM-dd',
        eventWidth: 'auto',
        collapsibleEvents: true
    });
    // 无时间时间线
    $('#noDateTimeline').kendoTimeline({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/timeline.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                data: 'data',
                model: {
                    fields: {
                        date: { type: 'date' }
                    }
                }
            }
        },
        dateFormat: 'yyyy-MM-dd',
        eventWidth: 'auto',
        showDateLabels: false
    });
    // 自定义时间线
    $('#customTimeline').kendoTimeline({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/timeline.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                data: 'data',
                model: {
                    fields: {
                        date: { type: 'date' }
                    }
                }
            }
        },
        dateFormat: 'yyyy-MM-dd',
        eventWidth: 'auto',
        collapsibleEvents: true,
        eventTemplate: kendo.template(
            '<div class="k-card-header" style="border-top: 6px solid #= data.color #;">' +
                '<div class="media">' +
                    '# for (var i = 0; i < data.images.length; i++) { #' +
                        '<img class="mr-3" src="#= data.images[i].src #" style="height: 48px;">' +
                    '# } #' +
                    '<div class="media-body">' +
                        '<h5 class="k-card-title mb-0">' +
                            '<span class="k-event-title">#= data.title #</span>' +
                            '<span class="k-event-collapse k-button k-button-icon k-flat">' +
                                '<span class="k-icon k-i-arrow-chevron-right"></span>' +
                            '</span>' +
                        '</h5>' +
                        '<h6 class="k-card-subtitle mb-0">#= data.subtitle #</h6>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="k-card-body">' +
                '<div class="k-card-description">' +
                    '<p>#= data.description #</p>' +
                '</div>' +
            '</div>' +
            '<div class="k-card-actions">' +
                '# for (var i = 0; i < data.actions.length; i++) { #' +
                    '<a class="k-button k-flat k-primary" href="#= data.actions[i].url #">#= data.actions[i].text #</a>' +
                '# } #' +
            '</div>'
        )
    });
});